import os
import time

from grap_tools import GrapTools
from odoo_restore_drop_obsolete_config import (
    INSTANCE_SETTINGS,
    ODOO_RELATED_SERVICES,
    POSTGRES_JOBS,
    POSTGRES_SLEEP_DURATION,
)

##############################################################################
# Settings Part
##############################################################################


##############################################################################
# Main Class
##############################################################################
class OdooRestoreDropObsolete(GrapTools):

    _script_name = "odoo_restore_drop_obsolete"
    _script_description = "Odoo Restore Databases and Drop obsolete Databases"

    _pg_configuration = {
        "host": "localhost",
        "port": "5432",
        "database": "postgres",
        "user": INSTANCE_SETTINGS[0]["db_user"],
        "password": INSTANCE_SETTINGS[0]["db_password"],
    }

    def _get_files(
        self, parent_folder, prefix_filter="", suffix_filter="", remove_suffix=False
    ):
        files = os.listdir(parent_folder)
        if suffix_filter:
            files = [x for x in files if x.endswith(suffix_filter)]
        if prefix_filter:
            files = [x for x in files if x.startswith(prefix_filter)]
        if remove_suffix:
            return [x[: -len(suffix_filter)] for x in files]
        return files

    def run(self):
        # Stop Odoo and Nginx Service
        self.service("stop", "nginx", "service")

        for instance_setting in INSTANCE_SETTINGS:
            if instance_setting.get("odoo_service_name", False):
                self.service("stop", instance_setting["odoo_service_name"], "systemctl")

        # restart postgres to kill all current connexion, including
        # psql connexion that could make fail the drop database
        # process
        self.service("restart", "postgresql", "service")
        time.sleep(POSTGRES_SLEEP_DURATION)

        for instance_setting in INSTANCE_SETTINGS:
            pg_configuration = self._pg_configuration
            pg_configuration.update(
                {
                    "user": instance_setting["db_user"],
                    "password": instance_setting["db_password"],
                }
            )
            self._pg_close()
            self._pg_init(pg_configuration)

            # Check existing databases
            all_databases = self.pg_get_databases(instance_setting["db_user"])

            self.logger_info(
                f"Found {len(all_databases)} database(s) for user {instance_setting['db_user']}: {', '.join(all_databases)}"
            )

            # Check backup directory content
            directory = self.get_youngest_directory(instance_setting["backup_folder"])
            self.logger_info(f"The youngest directory is {directory.full_path}")

            to_restore_database_names = self._get_files(
                directory.full_path,
                prefix_filter=instance_setting["db_prefix"],
                suffix_filter=".dump",
                remove_suffix=True,
            )
            self.logger_info(
                f"Database(s) to restore : {', '.join(to_restore_database_names)}"
            )

            # contains the list of still restore databases
            conflicted_databases = []

            for database_name in to_restore_database_names:
                database_root_name = database_name.split("__")[0]

                if database_name in all_databases:
                    # The database still exists.
                    # It mean that there is no new backup.
                    # an error will be raised a the end of the script
                    conflicted_databases.append(database_name)
                    continue

                # 1) Restore file System (for each DB to restore)
                tar_path = os.path.join(directory.full_path, database_name + ".tar.gz")

                filestore_path = os.path.join(
                    instance_setting["filestore_folder"], "filestore", database_name
                )

                self.create_folder(filestore_path, drop_if_exists=True)

                self.execute_shell(
                    f"sudo tar -xf {tar_path} --directory {filestore_path}"
                )
                self.execute_shell(
                    f"sudo chown -R {instance_setting['db_user']} {filestore_path}"
                )
                self.execute_shell(f"sudo chmod -R 777 {filestore_path}")

                # 2) Create and restore database (for each DB to restore)
                self.pg_create_database(database_name, instance_setting["db_user"])

                self.pg_restore(
                    database_name,
                    instance_setting["db_user"],
                    os.path.join(directory.full_path, database_name + ".dump"),
                    jobs=POSTGRES_JOBS,
                )

                self.pg_switch_database(database_name)

                # Disable Crons
                self.pg_execute(
                    """
                    UPDATE ir_cron
                    SET active=False
                    WHERE id != 1 and active=True;
                """
                )

                # Set to pending started / enqueued jobs
                # https://github.com/OCA/queue/tree/12.0/queue_job#known-issues--roadmap
                self.pg_execute(
                    """
                    UPDATE queue_job
                    SET state='pending'
                    WHERE state in ('started', 'enqueued');
                """
                )

                # Drop old databases
                for database in [x.lower() for x in all_databases]:
                    # We will delete all previous restoration of database
                    # with the same root_name. EX :
                    # if we are restoring caap_production__2021_02_17__03_20_01
                    # we will drop all previous caap_production__xxx databases
                    if not database.startswith(database_root_name):
                        continue
                    elif database.endswith("_production"):
                        self.logger_info(
                            "Database %s has not been deleted because it ends"
                            " with '_production' pattern" % (database)
                        )
                        continue
                    elif database in [
                        x.lower() for x in instance_setting.get("keep_databases", [])
                    ]:
                        self.logger_info(
                            "Database %s has not been deleted because it is"
                            " set to be conserved in the"
                            " file %s"
                            % (database, "odoo_restore_drop_obsolete_config.py")
                        )
                        continue
                    else:
                        self.pg_drop_database(database)
                        self.delete_folder_if_exists(
                            os.path.join(
                                instance_setting["filestore_folder"],
                                "filestore",
                                database,
                            )
                        )

                # Update all, if required
                if instance_setting.get("update_all", False):
                    self.execute_shell(
                        f"sudo su {instance_setting['db_user']} -c "
                        f" ' {instance_setting['python_path']} {instance_setting['odoo_command']} "
                        f" --config {instance_setting['odoo_config_path']} "
                        f" --database {database_name} "
                        " --workers=0  --update=all --stop-after-init'",
                        cwd=instance_setting["odoo_folder"],
                    )

        # Restart Odoo and Nginx Service
        for instance_setting in INSTANCE_SETTINGS:
            if instance_setting.get("odoo_service_name", False):
                self.service(
                    "start", instance_setting["odoo_service_name"], "systemctl"
                )
        self.service("start", "nginx", "service")

        # Restart related services
        for related_service in ODOO_RELATED_SERVICES:
            self.service("restart", related_service, "systemctl")

        if conflicted_databases:
            # Some databases has not been created.
            # raise a lazzy error to send a mail.
            self.logger_error(
                f"Databases already exists : {', '.join(conflicted_databases)}. "
                " Is the backup process OK ?"
            )


if __name__ == "__main__":
    OdooRestoreDropObsolete().run()
