from grap_tools import GrapTools
from postgres_vacuum_databases_config import DB_CREDENTIALS

##############################################################################
# Settings Part
##############################################################################


##############################################################################
# Main Class
##############################################################################
class PostgresVacuumDatabases(GrapTools):

    _script_name = "postgres_vacuum_databases"
    _script_description = "Postgres VACUUM Databases"

    _pg_configuration = {
        "host": "localhost",
        "port": "5432",
        "database": "postgres",
        "user": DB_CREDENTIALS[0]["user"],
        "password": DB_CREDENTIALS[0]["password"],
    }

    def run(self):
        for credential in DB_CREDENTIALS:
            databases = self.pg_get_databases(credential["user"])

            # Execute Query for each database
            for database in databases:
                self.pg_switch_database(
                    database, user=credential["user"], password=credential["password"]
                )
                self.pg_execute("VACUUM ANALYZE;")


if __name__ == "__main__":
    PostgresVacuumDatabases().run()
