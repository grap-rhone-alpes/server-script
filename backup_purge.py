from backup_purge_config import BACKUP_SETTINGS

from grap_tools import GrapTools

##############################################################################
# Settings Part
##############################################################################


##############################################################################
# Main Class
##############################################################################
class BackupPurge(GrapTools):

    _script_name = "backup_purge"
    _script_description = "Purge Backup Folders"

    def run(self):
        for backup_folder, backup_qty in BACKUP_SETTINGS.items():
            self.logger_info(
                "Checking that the backup quantity doesn't exceed %s in %s"
                % (backup_qty, backup_folder)
            )
            directory_list = self.get_directories(
                backup_folder, order="creation_timestamp"
            )

            if len(directory_list) > backup_qty:
                for directory in directory_list[:-backup_qty]:
                    self.execute_shell("sudo rm -r %s" % (directory.full_path))
            else:
                self.logger_info(
                    "Nothing to delete. Current Qty: %d / Max Qty: %d"
                    % (len(directory_list), backup_qty)
                )


if __name__ == "__main__":
    BackupPurge().run()
