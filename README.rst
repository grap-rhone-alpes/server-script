Installation
============

.. code-block:: shell

    # Pull Code
    git clone https://gitlab.com/grap-rhone-alpes/server-script
    cd server-script

    # Create virtualenv
    virtualenv env --python=python3

    # Install requirements
    ./env/bin/pip3 install -r requirements.txt

Configuration
=============

Edit a ``grap_tools_config.py`` file with the following content

.. code-block:: python

    # All scripts will log into a single file
    LOG_SCRIPT_PATH = "PATH_TO_LOG"

    # If defined, a mail is send if a log ``error`` or ``critical``
    # is done.
    SMTP_SERVER = "XXX"
    SMTP_PORT = XXX
    SMTP_LOGIN = "XXX"
    SMTP_PASSWORD = "XXX"
    SMTP_FROM = "XXX"
    SMTP_TO_LIST = ["XXX"]

Execution
=========

You can then run any scripts this way:

.. code-block:: shell

    ./env/bin/python ./postgres_vacuum_databases.py

**Note**

* If a file ``postgres_vacuum_databases.disabled`` is present, the script will be skipped.

* If a file ``postgres_vacuum_databases.email`` is present, an email will be send
  at the end of the execution. (Otherwise, the email will be sent only if a ``error``
  or a ``critical`` is raised.)


Scripts Details
===============

postgres_vacuum_databases.py
----------------------------

This python script:

* run a non blocking vacuum command on each postgreSQL database of a given
  user.

**Configuration**

Edit a ``postgres_vacuum_databases_config.py`` file with the following content:

.. code-block:: python

    DB_CREDENTIALS = [
        {"user": "XXX", "password": "YYY"},
    ]

odoo_backup_send.py
-------------------

This python script:

- backup database DATABASES and their related filestore into a
  local BACKUP_FOLDER.
- send by ``scp`` the full backup on other servers. (spare, backup, test.)

**Configuration**

Edit a ``odoo_backup_send_config.py`` file with the following content:

.. code-block:: python

    # Folder where the backup will be stored locally
    BACKUP_FOLDER = "XXX"

    # Odoo configuration of each instance
    INSTANCE_SETTINGS = [
        {
            "db_user": "odoo12",
            "db_password": "odoo",
            "filestore_folder": "FILESTORE_PATH",
            "odoo_service_name": "OPTIONAL_SERVICE_NAME",
        },
    ]

    # List of user@server where the backup will be pushed by scp
    PUSH_SERVERS = [
        "XXX@XXX",
        "YYY@YYY",
    ]


backup_purge.py
---------------


This python script:

- purge the backup folders to let only a given quantity of last backups.

**Configuration**

Edit a ``backup_purge_config.py`` file with the following content:

.. code-block:: python

    BACKUP_SETTINGS = {
        "PATH_1": QTY_1,
        "PATH_2": QTY_2,
    }


odoo_restore_drop_obsolete.py
-----------------------------

This python script:

- restores the last backups present in ``BACKUP_FOLDER``
- deletes all databases owned by ``db_user`` that contains the ``db_prefix`` of the restored databases
  and are not in the ``keep_databases`` list.
  Ex: if you try to restore ``grap_production__2021_02_17__03_20_01``, it will
  drop all databases (and filestore) of odoo instance that starts with ``grap_production``.

This script should be typically executed by cron on test and preproduction odoo servers.

Note if ``update_all`` is True, odoo will perform a full update=all after restoring the database.

**Configuration**

Edit a ``odoo_restore_drop_obsolete_config.py`` file with the following content:

.. code-block:: python

    # Folders where the backup are stored locally to be restored
    BACKUP_FOLDERS = [
        "PATH_TO_BACKUP_FOLDER_1",
    ]

    # time the postgres service must wait after a restart
    POSTGRES_SLEEP_DURATION = 10

    # number of jobs used for the pg_restore function
    POSTGRES_JOBS = 8

    # Odoo configuration of each instance
    INSTANCE_SETTINGS = [
        {
            "db_user": "odoo12",
            "db_password": "odoo",
            "db_prefix": "PREFIX_",
            "filestore_folder": "FILESTORE_PATH",
            "odoo_service_name": "OPTIONAL_SERVICE_NAME",
            "keep_databases": [],
            "update_all": True,
            "odoo_folder": "ODOO_ROOT_FOLDER",
            "python_path": "RELATIVE_PYTHON_PATH",
            "odoo_command": "RELATIVE_ODOO_PATH",
            "odoo_config_path": "RELATIVE_CONFIG_PATH",
        },

    # List of related services to restart after a restore. Typically the eshop
    ODOO_RELATED_SERVICES = [
    ]
