import datetime
import os

from grap_tools import GrapTools
from odoo_backup_send_config import BACKUP_FOLDER, INSTANCE_SETTINGS, PUSH_SERVERS

##############################################################################
# Settings Part
##############################################################################


##############################################################################
# Main Class
##############################################################################
class OdooBackupSend(GrapTools):

    _script_name = "odoo_backup_send"
    _script_description = "Backup Odoo and Send on Other Servers"

    # usefull to list databases
    _pg_configuration = {
        "host": "localhost",
        "port": "5432",
        "database": "postgres",
        "user": INSTANCE_SETTINGS[0]["db_user"],
        "password": INSTANCE_SETTINGS[0]["db_password"],
    }

    def run(self):
        # Initialize script
        str_date = datetime.datetime.now().strftime("%Y_%m_%d__%H_%M_%S")
        folder_name = "sauvegarde_%s__%s" % (self.hostname, str_date)

        # Stop Odoo and Nginx Service
        self.service("stop", "nginx", "service")
        for instance_setting in INSTANCE_SETTINGS:
            if instance_setting.get("odoo_service_name", False):
                self.service("stop", instance_setting["odoo_service_name"], "systemctl")

        # Create main backup folder
        backup_folder_path = os.path.join(BACKUP_FOLDER, folder_name)
        self.create_folder(backup_folder_path)

        for setting in INSTANCE_SETTINGS:
            databases = self.pg_get_databases(setting["db_user"])

            self.logger_info(
                f"Found {len(databases)} databases for user {setting['db_user']}."
            )

            for database in databases:
                if not database.startswith(setting.get("db_prefix", "")):
                    self.logger_info(
                        f"Skip '{database}' because it doesn't match the pattern"
                    )
                    continue

                self.logger_info(f"Backup database and filestore for '{database}'...")
                file_name = "%s__%s" % (database, str_date)

                database_path = os.path.join(backup_folder_path, f"{file_name}.dump")
                filestore_path = os.path.join(backup_folder_path, f"{file_name}.tar.gz")

                # dump database
                self.pg_dump(database, setting["db_user"], database_path)

                # Backup filestore
                self.execute_shell(
                    "cd %s && tar -czf %s ./"
                    % (
                        os.path.join(
                            setting["filestore_folder"], "filestore", database
                        ),
                        filestore_path,
                    )
                )

        # Restart Odoo and Nginx Service
        for instance_setting in INSTANCE_SETTINGS:
            if instance_setting.get("odoo_service_name", False):
                self.service(
                    "start", instance_setting["odoo_service_name"], "systemctl"
                )
        self.service("start", "nginx", "service")

        # Push data to external servers
        for push_server_url in PUSH_SERVERS:
            self.push_folder_by_scp(backup_folder_path, push_server_url)


if __name__ == "__main__":
    OdooBackupSend().run()
