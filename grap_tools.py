import atexit
import datetime
import logging
import os
import smtplib
import socket
import stat
import subprocess
from email.mime.text import MIMEText

import psycopg2

from grap_tools_config import (
    LOG_SCRIPT_PATH,
    SMTP_FROM,
    SMTP_LOGIN,
    SMTP_PASSWORD,
    SMTP_PORT,
    SMTP_SERVER,
    SMTP_TO_LIST,
)

##############################################################################
# Settings Part
##############################################################################


def _on_end_of_script(self):
    self._pg_close()
    self._end_script = datetime.datetime.now()
    self.logger_info("End of script")
    if self._mail_to_send:
        self._send_mail()


##############################################################################
# Main Class
##############################################################################
class GrapTools:

    _pg_configuration = False
    _pg_cr = False
    _all_logs = []

    def __init__(self):
        self._begin_script = datetime.datetime.now()
        atexit.register(_on_end_of_script, self)
        self._logger_init()
        self.logger_info("Begin of script")
        self.hostname = socket.gethostname()
        self._pg_init()

        # Check if a mails should be allways sent
        _mail_file = self._script_name + ".email"
        self._mail_to_send = os.path.isfile(_mail_file)

        # Test if the script is disabled manually
        _quencher_file = self._script_name + ".disabled"
        if os.path.isfile(_quencher_file):
            self.logger_warn(
                "Exiting because the quencher file '%s' is present." % (_quencher_file)
            )
            exit()

    ###########################################################################
    # Logging Part
    ###########################################################################
    def _logger_init(self):
        logging.basicConfig(
            filename=LOG_SCRIPT_PATH,
            format="%(asctime)s %(name)s - %(levelname)s : %(message)s",
            level=logging.INFO,
        )
        self._logger = logging.getLogger(self._script_name)

    def _append_log(self, level, message):
        self._all_logs.append((level, message, datetime.datetime.now()))

    def logger_info(self, message):
        self._append_log(logging.INFO, message)
        self._logger.info(message)

    def logger_warn(self, message):
        self._append_log(logging.WARNING, message)
        self._logger.warn(message)

    def logger_error(self, message):
        message += "... Exiting."
        self._append_log(logging.ERROR, message)
        self._logger.error(message)
        self._mail_to_send = True
        exit()

    def logger_critical(self, message):
        message += "... Exiting."
        self._append_log(logging.CRITICAL, message)
        self._logger.critical(message)
        self._mail_to_send = True
        exit()

    def _send_mail(self):
        self.logger_info("Send mail.")
        max_level = logging.INFO
        max_message = "All is OK."
        for level, message, dt in self._all_logs:
            if level > max_level:
                max_level = level
                max_message = message
        if not SMTP_SERVER:
            self._logger.error(
                "Mail not send because Mail Configuration has not been set."
            )
        try:
            # Create message

            begin_script = self._begin_script.strftime("%d/%m/%Y - %H:%M:%S")
            end_script = self._end_script.strftime("%d/%m/%Y - %H:%M:%S")
            duration = str(self._end_script - self._begin_script)
            logs_detail = "\n".join(
                [
                    "%s - %s - %s"
                    % (
                        log_date.strftime("%H:%M:%S"),
                        logging.getLevelName(level),
                        message,
                    )
                    for level, message, log_date in self._all_logs
                ]
            )
            body = (
                "Main message : {level} - {message}\n\n"
                "Begin of script : {begin_script}\n"
                "End of script : {end_script}\n"
                "Duration : {duration}\n\n"
                "Details : \n{logs_detail}"
                "".format(
                    level=logging.getLevelName(max_level),
                    message=max_message,
                    begin_script=begin_script,
                    end_script=end_script,
                    duration=duration,
                    logs_detail=logs_detail,
                )
            )
            mail = MIMEText(body)
            mail["To"] = ",".join(SMTP_TO_LIST)
            mail["Subject"] = "[%s][%s] script %s (%s)" % (
                self.hostname,
                logging.getLevelName(max_level),
                self._script_name,
                self._script_description,
            )

            # Note: This code should be adapted, if smtp_port is not 465
            # (= SSL)
            connection = smtplib.SMTP_SSL(SMTP_SERVER, SMTP_PORT)
            connection.login(SMTP_LOGIN, SMTP_PASSWORD)
            connection.sendmail(SMTP_FROM, SMTP_TO_LIST, mail.as_string())
            connection.quit()

        except Exception as e:
            self._logger.critical(
                "Unable to send the error / critical log by email. %s" % e
            )

    def mail_and_exit(self, level, message):
        self._send_mail(level, message)

    ###########################################################################
    # PostgreSQL Part
    ###########################################################################
    def _pg_init(self, pg_configuration={}):
        if not pg_configuration:
            pg_configuration = self._pg_configuration
        if pg_configuration:
            try:
                self._pg_connection = psycopg2.connect(**pg_configuration)
                self._pg_connection.set_isolation_level(0)
                self._pg_cr = self._pg_connection.cursor()
                self.logger_info(
                    "Connected to the PostgreSQL server (database %s)"
                    % (pg_configuration["database"])
                )

            except Exception:
                conf = pg_configuration.copy()
                conf["password"] = "************"
                self.logger_critical(
                    "Failed to connect to the PostgreSQL server."
                    " Configuration %s" % (conf)
                )

    def _pg_close(self):
        if self._pg_cr:
            try:
                self._pg_connection.set_isolation_level(1)
                self._pg_cr.close()
            except Exception:
                self.logger_warn(
                    "unable To close properly the PostgreSQL cursor" " in _pg_close()"
                )

    def pg_switch_database(self, database, user=False, password=False):
        self._pg_close()
        conf = self._pg_configuration.copy()
        conf["database"] = database
        if user:
            conf["user"] = user
        if password:
            conf["password"] = password

        self._pg_init(pg_configuration=conf)

    def pg_execute(self, request, action=False):
        request = " ".join([x.strip() for x in request.split("\n") if x.strip()])
        self.logger_info("Executing %s ..." % request)
        try:
            self._pg_cr.execute(request)
            if action == "fetchall":
                return True, self._pg_cr.fetchall()
            else:
                return True, False
        except Exception as e:
            self.logger_warn(
                "Failed to execute the request. %s" % (getattr(e, "message", ""))
            )
            if action == "fetchall":
                return False, []

    def pg_get_databases(self, owner):
        request = """
            SELECT pg_database.datname
            FROM pg_database
            INNER JOIN pg_user
            ON pg_user.usesysid = pg_database.datdba
            where usename = '%s';
            """ % (
            owner
        )
        _, result = self.pg_execute(request, action="fetchall")
        return [x[0] for x in result]

    def pg_create_database(self, db_name, db_user):
        request = """
            CREATE DATABASE %s owner %s;
            """ % (
            db_name,
            db_user,
        )
        self.pg_execute(request)

    def pg_drop_database(self, db_name):
        request = (
            """
            DROP DATABASE %s;
            """
            % db_name
        )
        self.pg_execute(request)

    def pg_dump(self, db_name, db_user, file_path):
        command = (
            f"sudo su {db_user} -c"
            f" 'pg_dump  {db_name} --file={file_path}"
            " --format=c'"
        )
        self.execute_shell(command)

    def pg_restore(self, db_name, db_user, file_path, jobs=1):
        command = (
            f"sudo su {db_user} -c "
            f"'pg_restore {file_path} --dbname={db_name} --jobs={jobs}"
            " --schema=public --no-owner'"
        )
        self.execute_shell(command)

    ###########################################################################
    # SCP Part
    ###########################################################################
    def push_folder_by_scp(self, folder_path, scp_url):
        self.execute_shell("scp -r %s %s:~/" % (folder_path, scp_url))

    ###########################################################################
    # File Part
    ###########################################################################
    def delete_folder_if_exists(self, folder_path):
        if os.path.isdir(folder_path):
            try:
                self.execute_shell("sudo rm -r %s" % (folder_path))
            except Exception:
                self.logger_error("Unable to drop folder %s." % folder_path)

    def create_folder(self, folder_path, drop_if_exists=False):
        if drop_if_exists:
            self.delete_folder_if_exists(folder_path)
        try:
            self.logger_info("Create folder %s (with mode 777)" % (folder_path))
            os.mkdir(folder_path)
            # Make the folder writable for all users
            # to allow for exemple, dump for user postgres
            os.chmod(folder_path, stat.S_IRWXU + stat.S_IRWXG + stat.S_IRWXO)
        except Exception:
            self.logger_error("Unable to create the folder %s." % folder_path)

    def get_youngest_directory(self, parent_folder):
        # # Get all directories
        directories = self.get_directories(
            parent_folder, order="creation_datetime", reverse=True
        )

        # Exit, if there are no folders
        if not directories:
            self.logger_critical("No folder found in  %s." % (parent_folder))

        # Get the most recent directory
        return directories[0]

    def get_directories(self, parent_folder, order=False, reverse=False):
        class Directory(object):
            pass

        self.logger_info("Scanning folder %s..." % parent_folder)

        if not os.path.isdir(parent_folder):
            self.logger_critical("Folder %s not found." % (parent_folder))
        # Get all directories
        directory_path_list = os.listdir(parent_folder)
        directory_path_list = [
            os.path.join(parent_folder, f)
            for f in directory_path_list
            if not os.path.isfile(os.path.join(parent_folder, f)) and not f[0] == "."
        ]

        self.logger_info("Found %d items" % len(directory_path_list))

        res = []
        for item in directory_path_list:
            obj = Directory()
            timestamp = os.path.getctime(item)
            setattr(obj, "full_path", item)
            setattr(obj, "creation_timestamp", timestamp)
            setattr(
                obj, "creation_datetime", datetime.datetime.fromtimestamp(timestamp)
            )

            res.append(obj)

        if order:
            res = sorted(res, key=lambda x: getattr(x, order), reverse=reverse)
        return res

    ###########################################################################
    # Service Part
    ###########################################################################
    def service(self, action, service_name, system):
        if system == "service":
            command = "sudo service %s %s" % (service_name, action)
        elif system == "systemctl":
            command = "sudo systemctl %s %s" % (action, service_name)
        self.execute_shell(command)

    ###########################################################################
    # System Part
    ###########################################################################

    def execute_shell(self, command, cwd="./"):
        # Note : Old call os.system(command)
        self.logger_info("Running command %s" % (command))
        try:
            return subprocess.check_output(command, cwd=cwd, shell=True)
        except Exception as e:
            self.logger_warn("Failed to execute the command. error %s" % (e))
