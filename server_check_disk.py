import logging
import shutil

import psutil

from grap_tools import GrapTools
from server_check_disk_config import DISK_SETTINGS

##############################################################################
# Settings Part
##############################################################################


##############################################################################
# Main Class
##############################################################################
class ServerCheckDisk(GrapTools):

    _script_name = "server_check_disk"
    _script_description = "Checks disk filling status"

    _DEFAULT_WARNING_THRESHOLD = 0.75
    _DEFAULT_ERROR_THRESHOLD = 0.90

    def run(self):
        _ignore_partition_types = [
            "squashfs",
        ]
        # Initialize script
        max_level = logging.INFO
        for partition in psutil.disk_partitions():
            if partition.fstype in _ignore_partition_types:
                continue
            info = shutil.disk_usage(partition.mountpoint)
            usage = info.used / info.total
            message = (
                f"Device : {partition.device} on '{partition.mountpoint}' ({partition.fstype})."
                f" Used : {info.used / (1024*1024*1024) :.1f}G - Free : {info.free / (1024*1024*1024) :.1f}G."
                f" Ratio : {usage * 100:.2f}%"
            )
            error_threshold = DISK_SETTINGS.get(partition.mountpoint, {}).get(
                "error_threshold", self._DEFAULT_ERROR_THRESHOLD
            )
            warning_threshold = DISK_SETTINGS.get(partition.mountpoint, {}).get(
                "warning_threshold", self._DEFAULT_WARNING_THRESHOLD
            )
            if usage >= error_threshold:
                max_level = max(max_level, logging.ERROR)
                message += f". ERROR Threshold : {error_threshold:.2f}%"
            elif usage >= warning_threshold:
                max_level = max(max_level, logging.WARNING)
                message += f". WARNING Threshold : {warning_threshold * 100:.2f}%"

            self.logger_info(message)

        if max_level == logging.WARNING:
            self.logger_warn("WARNING : one (or many) threshold have been reached.")
            self._mail_to_send = True
        elif max_level == logging.ERROR:
            self.logger_error("ERROR : one (or many) threshold have been reached.")


if __name__ == "__main__":
    ServerCheckDisk().run()
